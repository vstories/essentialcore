menu = NativeUI("Menu personnel")

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
            if IsControlJustPressed(1, 311) then
                if not Player.inMenu then
                    menu.title = "Menu personnel"
                    menu:Clear()   
                    menu:AddButton("Emote")
                    menu:AddButton("Inventaire", LoadMenu_Inventory)

                    menu.hidden = not menu.hidden                   
                else
                    menu.hidden = true
                    Player.inMenu = false
                end
            end
            
            menu:Render()
    end
end)