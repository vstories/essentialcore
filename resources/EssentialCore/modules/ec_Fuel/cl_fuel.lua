Citizen.CreateThread(function()
  local player = nil
  local vehicle = nil
  while true do
      Citizen.Wait(0)
      player = GetPlayerPed(-1)
      if IsPedInAnyVehicle(player, true) then

        local vehicle = GetVehiclePedIsUsing(player)
        if Fuel.ExistFuelLevel(vehicle) then
          Citizen.Trace("Fuel: exist \n")
          Fuel.ConsumeFuel(vehicle)
        else
          Citizen.Trace("Fuel: not exit \n")
          Fuel.SetFuelLevel(vehicle, 1000.0)
        end

        local fuelLevel = Fuel.GetFuelLevel(vehicle)
        Citizen.Trace("Fuel: " .. fuelLevel .. "\n")
      end
  end
end)
