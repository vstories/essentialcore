-- Constructor
Fuel = {}
Fuel.__index = Fuel

-- Meta table for users
setmetatable(Fuel, {
	__call = function(self, vehicleId)
		local p = {}
		
		p.vehicleId = vehicleId
    p.fuelAccelerationImpact = 0.0002;
    p.fuelTractionImpact = 0.0001;
    p.fuelRPMImpact = 0.0005;

		return setmetatable(p, Fuel)
	end
})


function Fuel:GetBaseFuel()
  IS_THIS_MODEL_A_CAR
end

function Fuel:SetFuelLevel(vehicle)
  if (IsThisModelACar(GetEntityModel(veh))) then
 
    DecorSetFloat(vehicle, "_ec_fuelLevel", fuelLevel)
end

function Fuel.GetFuelLevel(vehicle)
  return DecorGetFloat(vehicle, "_ec_fuelLevel")
end

function Fuel.InitFuelLevel(vehicle)
  DecorRegister("_ec_fuelLevel", 1)
end

function Fuel.ExistFuelLevel(vehicle)
  return DecorExistOn(vehicle, "_ec_fuelLevel")
end

-- TMP

--

function Fuel.ConsumeFuel(vehicle)
  if IsVehicleEngineOn(vehicle) then
      local fuelLevel = Fuel.GetFuelLevel(vehicle)
      local speed = GetEntitySpeed(vehicle)
      local acceleration = GetVehicleAcceleration(vehicle)
      local maxTraction = GetVehicleMaxTraction(vehicle)

      fuelLevel = fuelLevel - (speed * fuelRPMImpact)
      Citizen.Trace("Fuel : " .. (speed + fuelRPMImpact) .. "\n")
      fuelLevel = fuelLevel - (acceleration * fuelAccelerationImpact)
      Citizen.Trace("Fuel : " .. (acceleration * fuelAccelerationImpact) .. "\n")
      fuelLevel = fuelLevel - (maxTraction * fuelTractionImpact)
      Citizen.Trace("Fuel : " .. (maxTraction * fuelTractionImpact) .. "\n")

      Fuel.SetFuelLevel(vehicle, fuelLevel)
  end
end

Fuel.FuelStation = function(vehicle)



end
