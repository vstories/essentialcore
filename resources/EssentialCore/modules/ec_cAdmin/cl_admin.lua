
-- Teleport to player
RegisterNetEvent('ec:teleportToPlayer')
AddEventHandler('ec:teleportToPlayer', function (user)
    local player = GetPlayerFromServerId(user)
    local ped = GetPlayerPed(player)
    local pos = GetEntityCoords(ped)
    local me = GetPlayerPed(-1)
    
    RequestCollisionAtCoord(pos.x, pos.y, pos.z)
    while not HasCollisionLoadedAroundEntity(me) do
		RequestCollisionAtCoord(pos.x, pos.y, pos.z)
		Citizen.Wait(0)
	end
	
	SetEntityCoords(GetPlayerPed(-1), pos) -- Tp
end)
