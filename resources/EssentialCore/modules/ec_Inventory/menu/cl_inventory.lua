function LoadMenu_Inventory(args)
    menu.title = "Inventaire"
    menu:Clear()
    menu.config.width = 0.4
    menu:SetHeader({"Nom", "slot"})
    for itemId, item in pairs(Player.inventory.items) do
        menu:AddButton({item.name .. " (x" .. item.quantity  ..")", item.slot}, LoadMenu_Inventory_item, {itemId = itemId, item = item})
    end
end

function LoadMenu_Inventory_item(args)
    menu.title = args.item.name
    menu:Clear()
    if args.item.func ~= nil then
        menu:AddButton("Utiliser")
    end
    menu:AddButton("Donner", LoadInput_Inventory_GiveItem, {})
    menu:AddButton("Retour", LoadMenu_Inventory, {})
end

function LoadInput_Inventory_GiveItem(args)
    DisplayOnscreenKeyboard(1, "Quantité :", "", "", "", "", "", 3)
    if (GetOnscreenKeyboardResult()) then
            local res = tonumber(GetOnscreenKeyboardResult())
            TriggerServerEvent('ec:player:inventory:GiveItem', Player.source, args.itemId, args.item.quantity)
    end
end