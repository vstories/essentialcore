RegisterServerEvent('ec:player:GetInventory')
AddEventHandler('ec:player:GetInventory', function()
	TriggerClientEvent('ec:player:GetInventory', source, PlayerManager.players[source].inventory.items)
end)

RegisterServerEvent('ec:player:inventory:AddItem')
AddEventHandler('ec:player:inventory:AddItem', function(source, itemId, quantity)
    if PlayerManager.players[source] and PlayerManager.players[source].inventory then
	    PlayerManager.players[source].inventory:AddItem(itemId, quantity)
        TriggerClientEvent('ec:player:inventory:SetItem', source, itemId, PlayerManager.players[source].inventory.items[itemId])
    end
end)

RegisterServerEvent('ec:player:inventory:RemoveItem')
AddEventHandler('ec:player:inventory:RemoveItem', function(source, itemId, quantity)
    if PlayerManager.players[source] and PlayerManager.players[source].inventory then
	    PlayerManager.players[source].inventory:RemoveItem(itemId, quantity)
        TriggerClientEvent('ec:player:inventory:SetItem', source, itemId, PlayerManager.players[source].inventory.items[itemId])
    end
end)