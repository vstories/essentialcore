-- Constructor
Inventory = {}
Inventory.__index = Inventory

-- Meta table for users
setmetatable(Inventory, {
	__call = function(self, id)

		local p = {}
		p.items = self:GetInventory(id)
		p.slot_used = 0

		return setmetatable(p, Inventory)		
	end
})
 
function Inventory:GetInventory(userId)
	local query = MySQL:executeQuery("SELECT * FROM items WHERE userId = '@userId'", { ['@userId'] = userId })
	local result = MySQL:getResults(query)
	local items = {}

	for key, value in pairs(result) do
		items[value.itemId] = Item(ItemManager.items[value.itemId].name, ItemManager.items[value.itemId].slot, value.quantity)
	end

	return items
end

function Inventory:AddItem(itemId, quantity)
	if self.items[itemId] == nil then
		self.items[itemId] = Item(ItemManager.items[itemId].name, ItemManager.items[itemId].slot, quantity)
		MySQL:executeQuery("INSERT INTO items VALUES ('@itemId', '@userId', @quantity)", { ['@userId'] = self.data.id, ['@itemId'] = itemId, ['@quantity'] = quantity })
	else
		self.items[itemId]:Add(quantity)
		self:UpdateItemInDB(itemId)
	end
end

function Inventory:RemoveItem(itemId, quantity)
	if self.items[itemId] ~= nil then
		self.items[itemId]:Remove(quantity)
		if (self.items[itemId].quantity == 0) then
			MySQL:executeQuery("DELETE FROM items WHERE userId = '@userId' AND itemId = '@itemId'", { ['@userId'] = self.data.id, ['@itemId'] = itemId})
			self.items[itemId] = nil
		else
			self:UpdateItemInDB(itemId)
		end
	end
end

function Inventory:UpdateItemInDB(itemId)
	MySQL:executeQuery("UPDATE items SET quantity = '@quantity' WHERE UserId = '@userId' AND itemId = '@itemId'", { ['@userId'] = self.data.id, ['@itemId'] = itemId, ['@quantity'] = self.items[itemId].quantity })
end

PlayerManager:AddModule("Inventory")