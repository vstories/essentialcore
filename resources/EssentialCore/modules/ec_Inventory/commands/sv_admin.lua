
CommandManager:AddCommand("AddItem", 2, function (source, args)
    local itemId = tonumber(args[2])
    local quantity = tonumber(args[3])
    if ItemManager.items[itemId] ~= nil then
        PlayerManager.players[source].inventory:AddItem(itemId, quantity)
        TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, ItemManager.items[itemId].name .. "x" ..  quantity .. " added!")
    else
        TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, "Item : " .. itemId .. " not exist!")
    end
end)

CommandManager:AddCommand("RemoveItem", 2, function (source, args)
    local itemId = tonumber(args[2])
    local quantity = tonumber(args[3])
    if ItemManager.items[itemId] ~= nil then
        PlayerManager.players[source].inventory:RemoveItem(itemId, quantity)
        TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, ItemManager.items[itemId].name .. " x" ..  quantity .. " removed!")
    else
        TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, "Item : " .. itemId .. " not exist!")
    end
end)

CommandManager:AddCommand("Inventory", 2, function (source, args)
        tprint(PlayerManager.players[source].inventory)   
end)