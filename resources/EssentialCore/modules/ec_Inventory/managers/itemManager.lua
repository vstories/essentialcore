ItemManager = {}
ItemManager.__index = ItemManager

setmetatable(ItemManager, {
	__call = function(self)
		local p = {}
		
        p.items = {}
        
		return setmetatable(p, ItemManager)
	end
})

-- Create a item
function ItemManager:AddItem(id, name, slot, callback)
   self.items[id] = {
        name = name, 
        slot = slot, -- Number of Slot require
        callback = callback -- Function
   }
end

ItemManager = ItemManager()