-- Constructor
Item = {}
Item.__index = Item

-- Meta table for users
setmetatable(Item, {
	__call = function(self, name, slot, quantity)
		local p = {}
		
		p.name = name
		p.slot = slot
        p.quantity = quantity

		return setmetatable(p, Item)
	end
})

function Item:Add(quantity)
    self.quantity = self.quantity + quantity
end

function Item:Remove(quantity)
    local newQuantity = self.quantity - quantity
    if newQuantity < 0 then
        self.quantity = 0
    else
        self.quantity = newQuantity
    end
end