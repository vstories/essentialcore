Citizen.CreateThread(function()
    
    -- Get Player Id
    local playerId = PlayerId()
    
    while true do
        Citizen.Wait(0)
    
        -- Check level
        if GetPlayerWantedLevel(playerId) ~= 0 then
            SetPlayerWantedLevel(playerId, 0, false)
            SetPlayerWantedLevelNow(playerId, false)
        end
    end
end)