RegisterNetEvent('ec:garage:ShowGarage')
AddEventHandler('ec:garage:ShowGarage', function(vehicles, coords, args)
        if menu_garage == nil then
            menu_garage = NativeUI("Garage")
        end

        if not Player.inMenu then
            local closestVehicle = GetClosestVehicle(Player.blips['garage_inside'].x, Player.blips['garage_inside'].y, Player.blips['garage_inside'].z, 3.000, 0, 70)
            menu_garage:Clear()
            menu_garage.title = "Garage"
            menu_garage.config.width = 0.4   
            menu_garage:SetHeader({"Nom", "Disponible"})

            if DoesEntityExist(closestVehicle) then
                menu_garage:AddButton("Rentrer le vehicule", function()
                    if DecorExistOn(closestVehicle, "_ec_owner") and DecorGetInt(closestVehicle, "_ec_owner") == Player.data.id then
                        TriggerServerEvent('ec:garage:ReturnVehicle', DecorGetInt(closestVehicle, "_ec_id"))
                        Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(closestVehicle))
                        menu_garage.hidden = true
                        Player.inMenu = false
                    else
                        UI.drawNotification("Ce véhicule ne vous appartient pas")
                    end
                end)
            end

            menu_garage:AddButton("Sortir un vehicule", function()
                menu_garage:Clear()
                menu_garage.title = "Mes vehicules"
                menu_garage.config.width = 0.4   
                menu_garage:SetHeader({"Nom", "Disponible"})
                for id, vehicle in pairs(vehicles) do
                    local stateString = vehicle.state == 0 and "Oui" or "Non"

                    menu_garage:AddButton({vehicle.name, stateString}, function()
                        if vehicle.state > 0 then
                            UI.drawNotification("Ce véhicule n'est pas dans le garage")
                        else
                            if DoesEntityExist(closestVehicle) then
                                UI.drawNotification("La zone est encombrée") 
                            else
                                TriggerServerEvent('ec:garage:SpawnVehicle', vehicle)
                            end
                        end
                    end)
                end
            end)

            menu_garage.hidden = not menu_garage.hidden

            Citizen.CreateThread(function()
                while not menu_garage.hidden do
                    Citizen.Wait(0)  
                    
                    if GetDistanceBetweenCoords(coords.x, coords.y, coords.z, GetEntityCoords(GetPlayerPed(-1))) > 5 then
                        menu_garage.hidden = true
                        break
                    end
                    
                    menu_garage:Render()
                end
                
                Player.inMenu = false
            end)

        else
            menu_garage.hidden = true
            Player.inMenu = false
        end
end)

RegisterNetEvent('ec:garage:SpawnVehicle')
AddEventHandler('ec:garage:SpawnVehicle', function(vehicle)
	local model_car = GetHashKey(vehicle.model)
	local plate = vehicle.plate
	local color1 = tonumber(vehicle.color1)
	local color2 = tonumber(vehicle.color2)

	Citizen.CreateThread(function()			
		Citizen.Wait(0)
            menu_garage.hidden = true
            Player.inMenu = false
            -- local mods = {}
            -- for i = 0,24 do
            -- 	mods[i] = GetVehicleMod(veh,i)
            -- end					
            RequestModel(model_car)
            while not HasModelLoaded(model_car) do
                Citizen.Wait(0)
            end
            createdVehicle = CreateVehicle(model_car, Player.blips['garage_inside'].x, Player.blips['garage_inside'].y, Player.blips['garage_inside'].z, 0.0, true, false)
            -- for i,mod in pairs(mods) do
            -- 	SetVehicleModKit(personalvehicle,0)
            -- 	SetVehicleMod(personalvehicle,i,mod)
            -- end
            SetVehicleNumberPlateText(createdVehicle, plate)
            SetVehicleOnGroundProperly(createdVehicle)
            SetVehicleHasBeenOwnedByPlayer(createdVehicle, true)
            DecorRegister("_ec_owner", 3)
            DecorSetInt(createdVehicle, "_ec_owner", Player.data.id)
            DecorRegister("_ec_id", 3)
            DecorSetInt(createdVehicle, "_ec_id", vehicle.id)
            local id = NetworkGetNetworkIdFromEntity(createdVehicle)
            SetNetworkIdCanMigrate(id, true)
            SetVehicleColours(createdVehicle, color1, color2)
            --SetVehicleExtraColours(createdVehicle, pearlescentcolor, wheelcolor)
            SetEntityInvincible(createdVehicle, false)
		    SetEntityAsMissionEntity(createdVehicle, true, true)	
			
	end)
end)