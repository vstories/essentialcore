RegisterServerEvent('ec:garage:ShowGarage')
AddEventHandler('ec:garage:ShowGarage', function(coords)
    TriggerClientEvent('ec:garage:ShowGarage', source, PlayerManager.players[source].garage.vehicles, coords)
end)


RegisterServerEvent('ec:garage:SpawnVehicle')
AddEventHandler('ec:garage:SpawnVehicle', function(vehicle)
    PlayerManager.players[source].garage:ChangeVehicleState(vehicle.id, 1)
    TriggerClientEvent('ec:garage:SpawnVehicle', source, vehicle)
end)

RegisterServerEvent('ec:garage:ReturnVehicle')
AddEventHandler('ec:garage:ReturnVehicle', function(vehicleId)
    PlayerManager.players[source].garage:ChangeVehicleState(vehicleId, 0)
end)