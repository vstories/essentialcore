-- Constructor
Garage = {}
Garage.__index = Garage

-- Meta table for users
setmetatable(Garage, {
	__call = function(self, id)

		local p = {}
		p.id = id
		p.vehicles = self:GetGarage(id)
		
		return setmetatable(p, Garage)		
	end
})
 
function Garage:GetGarage(userId)
	local query = MySQL:executeQuery("SELECT * FROM vehicles WHERE ownerId = '@userId'", { ['@userId'] = userId })
	local result = MySQL:getResults(query)
	local vehicles = {}
	for k, vehicle in pairs(result) do
		vehicles[vehicle.id] = vehicle
	end

	return vehicles
end

function Garage:ChangeVehicleState(vehicleId, state)
	if self.vehicles[vehicleId] ~= nil then
		self.vehicles[vehicleId].state = state
		MySQL:executeQuery("UPDATE vehicles SET state = '@state' WHERE ownerId = '@userId' and id = '@vehicleId'", { ['@userId'] = self.id, ['@vehicleId'] = vehicleId, ['@state'] = state })
	end
end

-- function Garage:Addvehicle(vehicleId)
-- 	if self.items[itemId] == nil then
-- 		self.items[itemId] = Item(ItemManager.items[itemId].name, ItemManager.items[itemId].slot, quantity)
-- 		MySQL:executeQuery("INSERT INTO vehicles VALUES ('@vehicleId', '@userId', '@model', '@isSpawned')", { ['@userId'] = self.id, ['@vehicleId'] = vehicleId, ['@model'] = model, ['@isSpawned'] = isSpawned })
-- 	else
-- 		self.items[itemId]:Add(quantity)
-- 		self:UpdateItemInDB(itemId)
-- 	end
-- end

-- function Garage:Removevehicle(vehicleId)
-- 	if self.items[itemId] ~= nil then
-- 		self.items[itemId]:Remove(quantity)
-- 		if (self.items[itemId].quantity == 0) then
-- 			MySQL:executeQuery("DELETE FROM vehicles WHERE userId = '@userId' AND vehicleId = '@vehicleId'", { ['@userId'] = self.id, ['@vehicleId'] = vehicleId})
-- 			self.items[itemId] = nil
-- 		else
-- 			self:UpdateItemInDB(itemId)
-- 		end
-- 	end
-- end

-- function Garage:UpdateItemInDB(itemId)
-- 	MySQL:executeQuery("UPDATE items SET quantity = '@quantity' WHERE UserId = '@userId' AND itemId = '@itemId'", { ['@userId'] = self.id, ['@itemId'] = itemId, ['@quantity'] = self.items[itemId].quantity })
-- end


PlayerManager:AddModule("Garage")

MarkerManager:AddMarker("garage_inside", "", 0, 0, false, 215.124, -791.377, 29.936, 10, true, true, 50, 3, nil)
MarkerManager:AddMarker("garage", "Garage", 0, 0, false, 213.769, -808.965, 29.914, 10, true, false, 50, 0, "ec:garage:ShowGarage")