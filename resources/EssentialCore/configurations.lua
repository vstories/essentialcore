Config = {}

-- Ping
Config.maxPing = 200
Config.pingKickText = "Kick for ping too high"
Config.timeCheck = 60000 -- 1 minute

-- Save Position
Config.SavePosition = 15000

-- Afk
Config.afkTime = 300 -- in seconds
Config.preventAfkKickText = "AFK!"
Config.kickAfkText = "Kick for Afk"

-- Only staff can connect server (true or false)
Config.onlyStaff = true

-- Default Spawn
Config.DefaultSpawn = {x = -1044.64538574219, y = -2749.82885742188, z = 21.3634281158447}

-- NUI configuration

Config.ShowUIData = { 
    cash = {"$", "green"},
    dirtyCash = "bank-icon.png",
    bank = "bank-icon.png",
    role = {"role", "white"}
}

Config.RoleName = {
    { "Cadet", "Officier", "Sergent", "Lieutenant", "Capitaine" }, -- 1
    { "Ambulancier", "Medecin" }, -- 2
    { "Gouvernement" }, -- 3
    { "Mineur" }, -- 4
    { "Taxi" }, -- 5
}