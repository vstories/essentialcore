resource_type 'gametype' { name = 'ESSENTIALCORE' }
description 'EssentialCore by V Stories.'

resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5' -- old
-- resource_manifest_version 'f15e72ec-3972-4fe4-9c7d-afc5394ae20' -- new

-- Scripts
client_scripts {

    -- Configurations --
    "configurations.lua",

    "core/client/utils.lua",

    -- Init
    "core/client/helpers/ui.lua",
    "core/client/helpers/nativeUI.lua",
    "core/client/managers/iplManager.lua",
    "core/client/init.lua",
 
    -- Events
    "core/client/events/playerEvent.lua",
    "core/client/events/vehicleEvent.lua",
    "core/client/events/markerEvent.lua",

    -- Threads

    --"core/client/threads/antiAfk.lua",
    --"core/client/threads/ping.lua",
    "core/client/threads/nui.lua",
    "core/client/threads/updatePositions.lua",
}

server_scripts {

    -- Configurations --
    "configurations.lua",

    -- Init --
    "core/server/utils.lua",
    "core/server/libs/mysql/database.lua",
    "core/server/models/player.lua",
    "core/server/managers/commandManager.lua",
    "core/server/managers/playerManager.lua",
    "core/server/managers/markerManager.lua",
    "core/server/init.lua",

    -- Events --
    "core/server/events/chatEvent.lua",
    "core/server/events/playerEvent.lua",
    "core/server/events/markerEvent.lua",
    "core/server/events/eventManager/playerManagerEvent.lua",
    "core/server/events/eventManager/markerManagerEvent.lua",
    "core/server/events/eventManager/vehicleManagerEvent.lua",
    "core/server/events/eventManager/commandManagerEvent.lua",

}

ui_page 'core/client/NUI/main.html'
--loadscreen 'modules/ec_LoadingScreen/loadingScreen.html'

files {
	'core/client/NUI/main.html',
    'core/client/NUI/pdown.ttf',  
    'modules/ec_LoadingScreen/loadingScreen.html',
    'modules/ec_LoadingScreen/loadingScreen.css',

    -- Resources

    'resources/bank-icon.png',
}

-- EssentialCore_Inventory

server_script("modules/ec_Inventory/models/item.lua")
server_script("modules/ec_Inventory/managers/itemManager.lua")
server_script("modules/ec_Inventory/sv_inventory.lua")
server_script("modules/ec_Inventory/commands/sv_admin.lua")
client_script("modules/ec_Inventory/menu/cl_inventory.lua")
client_script("modules/ec_Inventory/cl_init.lua")
server_script("modules/ec_Inventory/events/sv_inventoryEvent.lua")
client_script("modules/ec_Inventory/events/cl_inventoryEvent.lua")

-- EssentialCore_cAdmin

client_script("modules/ec_cAdmin/cl_admin.lua")

-- EssentialCore_noWanted

client_script("modules/ec_noWanted/cl_noWanted.lua")

-- EssentialCore_LplLoader

client_script("modules/ec_IplLoader/cl_main.lua")

-- EssentialCore_Fuel

--client_script("modules/ec_Fuel/cl_fuel.lua")
--client_script("modules/ec_fuel/cl_fuelService.lua")


-- EssentialCore_Garage

server_script("modules/ec_Garage/sv_garage.lua")
--client_script("modules/ec_Garage/cl_garage.lua")
client_script("modules/ec_Garage/events/cl_garageEvent.lua")
server_script("modules/ec_Garage/events/sv_garageEvent.lua")

-- V Stories

server_script("modules/V_Stories/commands/sv_admin.lua")
server_script("modules/V_Stories/items/sv_item.lua")
client_script("modules/V_Stories/menus/cl_menu.lua")