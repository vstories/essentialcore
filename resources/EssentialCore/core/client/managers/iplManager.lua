-- Constructor
IplManager = {}
IplManager.__index = IplManager

-- Meta table for users
setmetatable(IplManager, {
	__call = function(self)
		local p = {}
		
		p.listAdd = {}
		p.listRemove = {}
		
		return setmetatable(p, IplManager)
	end
})

function IplManager:Add(ipl)
    table.insert(self.listAdd, ipl)
end

function IplManager:Remove(ipl)
    table.insert(self.listRemove, ipl)
end

function IplManager:Init()
   
    LoadMpDlcMaps()
    EnableMpDlcMaps(true)
    
    -- Remove
    for _, value in pairs(self.listRemove) do
        RemoveIpl(value)
    end
    
    -- Add
    for _, value in pairs(self.listAdd) do
        RequestIpl(value)
    end
end