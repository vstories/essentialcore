-- Print tables
function tprint (tbl, indent)
  if not indent then indent = 0 end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      Citizen.Trace(formatting .. "\n")
      tprint(v, indent+1)
    else
      Citizen.Trace(formatting .. v .. "\n")
    end
  end
end
