UI = {}

UI.SendNotification = function(icon, type, sender, title, text)
        SetNotificationTextEntry("STRING")
        AddTextComponentString(text)
        SetNotificationMessage(icon, icon, true, type, sender, title, text)
        DrawNotification(false, true)
end


UI.DisplayHelpText = function(text, loop)
	SetTextComponentFormat("STRING")
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, loop, 1, -1)
end

UI.drawNotification = function(text)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(text)
	DrawNotification(false, false)
end

UI.DrawText = function(text, font, position, x, y, scale, r, g, b, a)
	SetTextFont(font)
	SetTextProportional(0)
	SetTextScale(scale, scale)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0,255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextCentre(position)
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x, y)
end