-- Constructor
NativeUI = {}
NativeUI.__index = NativeUI

-- Meta table for users
setmetatable(NativeUI, {
	__call = function(self, title, tableconfig)
		local p = {}
        local tableconfig = tableconfig or {}

		p.menu = {}
		p.title = title or "default menu"
        p.header = nil
        p.hidden = true
        p.maxElement = 6
        p.select = 1
        p.page = 1
        p.config = {
            width = 0.2,
            yoffset = tableconfig.yoffset or 0.3,
            xoffset = tableconfig.xoffset or 0.015,  
            title_paddingY = tableconfig.title_paddingY or 0.04,
            title_height = tableconfig.title_height or 0.08,
            title_BGColor = tableconfig.title_BGColor or {9,60,121,255},
            button_paddingY = tableconfig.button_paddingY or 0.05,
            button_height = tableconfig.button_height or 0.05,
            button_BGColor = tableconfig.button_BGColor or {0,0,0,220},
            button_ActiveBGColor = tableconfig.button_ActiveBGColor or {204,204,204,240},
            header_BGColor = tableconfig.button_BGColor or {0,0,0,255},
        }

		return setmetatable(p, NativeUI)
	end
})

function NativeUI:AddButton(value, func, args)
     table.insert(self.menu, {
            value = value,
            func = func,
            args = args or {},
            isActive = #self.menu == 0 and true or false
         })
end

function NativeUI:SetHeader(headers)
    if type(headers) == "table" then
        self.header = headers
     end
end

function NativeUI:Select()
    if IsControlJustPressed(1, 173) then -- DOWN
            
            self.menu[self.select].isActive = false
            if self.select < #self.menu then
                repeat
                    self.select = self.select + 1
                until self.menu[self.select].isActive ~= nil

                if self.select == (self.maxElement * self.page) + 1 then
                    self.page = self.page + 1
                end
            end

            Citizen.Trace("select : " .. self.select .. " page :" .. self.page)

            self.menu[self.select].isActive = true

	elseif IsControlJustPressed(1, 27) then -- TOP

            self.menu[self.select].isActive = false
            if self.select > 1 then
                repeat
                    self.select = self.select - 1
                until self.menu[self.select].isActive ~= nil

                if self.select == (self.maxElement * (self.page - 1)) and self.page ~= 1 then
                    self.page = self.page - 1
                end
            end

            Citizen.Trace("select : " .. self.select .. " page :" .. self.page)
            
            self.menu[self.select].isActive = true
		
	elseif IsControlJustPressed(1, 201) then -- ENTER
        if self.menu[self.select].func ~= nil then
            self.menu[self.select].func(self.menu[self.select].args)
        end
	end
end

function NativeUI:RenderTitle()
    SetTextFont(1)
    SetTextScale(1.0, 1.0)
    SetTextColour(255, 255, 255, 255)
    SetTextEdge(0, 0, 0, 0, 0)
    SetTextWrap(0.0000, self.config.xoffset + self.config.width)
    SetTextCentre(1)
    SetTextEntry("STRING") 

    local xMin = self.config.xoffset + (self.config.width / 2)
    local yMin = (self.config.title_paddingY - self.config.button_height) + self.config.yoffset

    if self.header ~= nil then
        yMin = yMin - self.config.button_height
    end
    

    AddTextComponentString(self.title)
    DrawText(xMin, yMin - 0.03)
    DrawRect(xMin, yMin, self.config.width, self.config.title_height, self.config.title_BGColor[1], self.config.title_BGColor[2], self.config.title_BGColor[3], self.config.title_BGColor[4])
end


function NativeUI:RenderHeader()
    SetTextFont(0)
    SetTextScale(0.0,0.35)
    SetTextColour(255, 255, 255, 255)
    SetTextEdge(0, 0, 0, 0, 0)
    SetTextEntry("STRING") 

    local xMin = self.config.xoffset
    local yMin = (self.config.title_paddingY - self.config.button_paddingY) + 0.0150 + self.config.yoffset

    if type(self.header) == "table" then
        AddTextComponentString(tostring(self.header[1]))
        DrawText(xMin + 0.015, (yMin - 0.0125))
        
       self:AddTextInRight(self.header[2], xMin, yMin)
    end

    DrawRect(self.config.xoffset + (self.config.width / 2), yMin, self.config.width, self.config.button_height, self.config.header_BGColor[1], self.config.header_BGColor[2], self.config.header_BGColor[3], self.config.header_BGColor[4])
end

function NativeUI:RenderButton(key, element)
    SetTextFont(0)
    SetTextScale(0.0,0.35)
    SetTextColour(255, 255, 255, 255)
    SetTextEdge(0, 0, 0, 0, 0)
    SetTextEntry("STRING") 
    

    local xMin = self.config.xoffset
    local yMin = self.config.button_paddingY * (key + 0.01) + self.config.yoffset

    local color1, color2, color3, color4 = self.config.button_BGColor[1], self.config.button_BGColor[2], self.config.button_BGColor[3], self.config.button_BGColor[4]

    if element.isActive then
        SetTextColour(0, 0, 0, 255)
        color1, color2, color3, color4 = self.config.button_ActiveBGColor[1], self.config.button_ActiveBGColor[2], self.config.button_ActiveBGColor[3], self.config.button_ActiveBGColor[4]
    end

    if type(element.value) == "table" then

        AddTextComponentString(tostring(element.value[1]))
        DrawText(xMin + 0.015, (yMin - 0.0125 ))
        
       self:AddTextInRight(element.value[2], xMin, yMin)
    else
        AddTextComponentString(tostring(element.value))
        DrawText(xMin + 0.015, (yMin - 0.0125 )) 
    end
    
    DrawRect(self.config.xoffset + (self.config.width / 2), yMin, self.config.width, self.config.button_height, color1, color2, color3, color4);
end

function NativeUI:AddTextInRight(text, xMin, yMin)
    SetTextFont(0)
    SetTextScale(0.0,0.35)
    SetTextColour(255, 255, 255, 255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextRightJustify(1)
    SetTextWrap(0.0000, self.config.width)
    SetTextEntry("STRING")   
    AddTextComponentString(tostring(text))
    DrawText(xMin - 0.08, (yMin - 0.0125 ))
end

function NativeUI:Render()
    if not self.hidden then
        if Player.inMenu ~= nil and not Player.inMenu then
            Player.inMenu = true
        end
        
        self:RenderTitle()
        if self.header ~= nil then
            self:RenderHeader()
        end
        for key, element in pairs(self.menu) do
            if key <= (self.maxElement * self.page) and key >= ((self.maxElement * self.page) - self.maxElement) + 1 then
                self:RenderButton(self.maxElement - ((self.maxElement * self.page) - key), element)
            end
                
        end
        --self:RenderInfo()
        self:Select()
    end
end

function NativeUI:Clear()
    self.select = 1
    self.menu = {}
    self.header = nil
    self.page = 1
    self.config.width = 0.2
end
