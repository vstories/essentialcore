Citizen.CreateThread(function()
	local oldPos
	while true do
		Citizen.Wait(Config.SavePosition)
		local pos = GetEntityCoords(GetPlayerPed(-1), true)
		
		if (oldPos ~= pos) then
			TriggerServerEvent('ec:player:SetPlayer', {posX = pos.x, posY = pos.y, posZ = pos.z}, false)
        end
			oldPos = pos
	end
end)