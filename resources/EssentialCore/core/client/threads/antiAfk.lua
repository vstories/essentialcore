Citizen.CreateThread(function()
	
	local playerPed = nil
	local currentPos = nil

	while true do
		Wait(1000) -- 1 seconds
		
		playerPed = GetPlayerPed(-1)
		if playerPed then
			
			currentPos = GetEntityCoords(playerPed, true) -- Get plyer position
			
			if currentPos == prevPos then
				
				if time > 0 then
					if time == math.ceil(Config.afkTime / 4) then
						UI.SendNotification("CHAR_SOCIAL_CLUB", 1, "System", false, Config.preventAfkKickText)
					end

					time = time - 1
				else
					TriggerServerEvent("ec:player:kickPlayer", Config.kickAfkText)
				end
			else
				time = Config.afkTime
			end

			prevPos = currentPos
		end
	end
end)