-- Spawn override
AddEventHandler('onClientMapStart', function()
    exports.spawnmanager:setAutoSpawn(true)
    exports.spawnmanager:forceRespawn()
    exports.spawnmanager:setAutoSpawnCallback(function()
        if spawnLock then
            return
        end

        spawnLock = false

        TriggerServerEvent('playerSpawn')
        TriggerEvent('playerSpawn')
    
    end)
end)

-- Allows the server to spawn the player
RegisterNetEvent('ec:player:spawnPlayer')
AddEventHandler('ec:player:spawnPlayer', function(x, y, z)
    if x == 0 and y == 0 and z == 0 then
        x, y, z = Config.DefaultSpawn.x, Config.DefaultSpawn.y, Config.DefaultSpawn.z
    end
    
    exports.spawnmanager:spawnPlayer({x = x, y = y, z = z})
end)

-- Spawn player
OnPlayerSpawned = {}
AddEventHandler("playerSpawned", function(spawn)
    
    local player = GetPlayerPed(-1)
    TriggerServerEvent("ec:marker:GetMarkers")
    TriggerServerEvent('ec:player:GetPlayer')
    for k, eventName in pairs(OnPlayerSpawned) do
        TriggerServerEvent(eventName)
    end
    
    -- Enable pvp
	SetCanAttackFriendly(player, true, true)
	NetworkSetFriendlyFireOption(true)
end)

RegisterNetEvent('ec:player:SetPlayer')
AddEventHandler('ec:player:SetPlayer', function(data)
    for column, value in pairs(data) do
        if Player.data[column] ~= value then
		    Player.data[column] = value
            if Config.ShowUIData[column] ~= nil then
                SendNUIMessage({
                    setData = true,
                    name = column,
                    label = Config.ShowUIData[column],
                    value = value
                })
            end
        end
	end
end)