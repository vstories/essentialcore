RegisterNetEvent('ec:marker:SetMarkers')
AddEventHandler('ec:marker:SetMarkers', function(markers)
    for name, marker in pairs(markers) do
        local blip = 0
        if marker.showBlip then
            blip = AddBlipForCoord(marker.coords.x, marker.coords.y, marker.coords.z)
            SetBlipSprite(blip, marker.blipSprite)
            SetBlipColour(blip, marker.blipColor)
            SetBlipAsShortRange(blip, true)
            BeginTextCommandSetBlipName("STRING")
            AddTextComponentString(marker.label)
            EndTextCommandSetBlipName(blip)
            SetBlipAsMissionCreatorBlip(blip, true)
        end

        Player.blips[name] = marker.coords

        Citizen.CreateThread(function()
            --local name = name
            while Player.blips[name] ~= nil do
                Wait(0)
                if marker.showMarker then
                    DrawMarker(1,marker.coords.x, marker.coords.y, marker.coords.z,0,0,0,0,0,0,2.001,2.0001,0.5001,0,155,255,200,0,0,0,0)
                end

                if marker.eventName ~= nil then
                    if GetDistanceBetweenCoords(marker.coords.x, marker.coords.y, marker.coords.z, GetEntityCoords(GetPlayerPed(-1))) < 5 then
                        UI.DrawText('~g~F1~s~ pour interagir',0,1,0.5,0.8,0.6,255,255,255,255)
                        if IsControlJustPressed(1, 288) then
                            TriggerServerEvent(marker.eventName, marker.coords, marker.eventArgs)
                        end
                    end
               end
            end
            
        end)
       
    end
end)

RegisterNetEvent('ec:RemoveMarkers')
AddEventHandler('ec:RemoveMarkers', function(markers)
    for key, name in pairs(markers) do
        if Player.blips[name] ~= nil then
            if Player.blips[name] ~= 0 then
                Citizen.InvokeNative(0x86A652570E5F25DD, Citizen.PointerValueIntInitialized(Player.blips[name]))
            end
            
            Player.blips[name] = nil
        end
    end
end)
