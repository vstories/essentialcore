-- Autor : KANERSPS --

--[[ MySQL Class

--]]

MySQL = setmetatable({}, MySQL)
MySQL.__index = MySQL

function MySQL.open(self, server, database, userid, password)
	local reflection = clr.System.Reflection
	local assembly = reflection.Assembly.LoadFrom('resources/EssentialCore/core/server/libs/mysql/MySql.Data.dll')
	self.mysql = clr.MySql.Data.MySqlClient
	self.connection = self.mysql.MySqlConnection("server="..server..";database="..database..";userid="..userid..";password="..password.."")
	self.connection.Open()
end

function MySQL.executeQuery(self, command, params)
	local c = self.connection.CreateCommand()
	c.CommandText = command

	if type(params) == "table" then
		for param in pairs(params) do
			c.CommandText = string.gsub(c.CommandText, param, self:escape(params[param]))
		end
	end

	local res = c.ExecuteNonQuery()
	print("Query Executed("..res.."): " .. c.CommandText)

	return {mySqlCommand = c, result = res}
end

-- Return result
function MySQL.getResult(self, mySqlCommand)

	if type(mySqlCommand) == "table" and mySqlCommand['mySqlCommand'] ~= nil then
		mySqlCommand = mySqlCommand['mySqlCommand']
	end

	local reader = mySqlCommand:ExecuteReader()
	local result = {}
	local count = 0 -- Count number result

	while reader:Read() do
		
		if count == 0 then
			local column = 0 -- Count column result
			while (column < reader:FieldCount()) do
				nameField = reader:GetName(column) -- Get name column
				result[nameField] = self:_getFieldByName(reader, column) -- Get value and convert to corect type
				column = column + 1
			end
		end
		count = count + 1
	end
	
	if count > 1 then
		print("[Info] result found :" .. count .. " -  Please check your request or use getResults function")
	end

	reader:Close()
	return result
end

-- Return all results
function MySQL.getResults(self, mySqlCommand)

	if type(mySqlCommand) == "table" and mySqlCommand['mySqlCommand'] ~= nil then
		mySqlCommand = mySqlCommand['mySqlCommand']
	end
	
	local reader = mySqlCommand:ExecuteReader()
	
	local result = {}
	local count = 0 -- Count number result

	while reader:Read() do
		result[count] = {}
		local column = 0 -- Count column result
		while (column < reader:FieldCount()) do
			nameField = reader:GetName(column) -- Get name column
			result[count][nameField] = self:_getFieldByName(reader, column) -- Get value and convert to corect type
			column = column + 1
		end
		count = count + 1
	end
	reader:Close()
	return result
end

function MySQL.escape(self, str)
	return self.mysql.MySqlHelper.EscapeString(str)
end

function MySQL._getFieldByName(self, reader, name)

	-- Super check
	if not reader:IsDBNull(name) then
		local typ = tostring(reader:GetFieldType(name))
		
		if(typ == "System.DateTime") then
			return reader:GetDateTime(name)
		elseif (typ == "System.Double") then
			return reader:GetDouble(name)
		elseif (typ == "System.Int32") then
			return reader:GetInt32(name)
		elseif (typ == "System.Boolean") then
			return reader:GetBoolean(name)
		elseif (typ == "System.String") then
			return reader:GetString(name)
		else
			return nil
		end
	else
		return nil
	end

end