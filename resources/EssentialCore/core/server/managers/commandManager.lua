-- Constructor
CommandManager = {}
CommandManager.__index = CommandManager

-- Meta table for users
setmetatable(CommandManager, {
	__call = function(self)
		local p = {}

	    p.commands = {}
		
		return setmetatable(p, CommandManager)
	end
})

-- Add commande
function CommandManager:AddCommand(prefix, permissionLevel, callback)
   self.commands[prefix] = {
        permissionLevel = permissionLevel, -- Min permision for use this command
        callback = callback -- Function
   }
end