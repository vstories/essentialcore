-- Constructor
MarkerManager = {}
MarkerManager.__index = MarkerManager

-- Meta table for users
setmetatable(MarkerManager, {
	__call = function(self)
		local p = {}

		p.markers = {}
		
		return setmetatable(p, MarkerManager)
	end
})


function MarkerManager:AddMarker(name, label, role, subrole, subroleStrict, posX, posY, posZ, range, showMarker, showBlip, blipSprite, blipColor, eventName, eventArgs)
	self.markers[name] = {
		label = label,
		role = role,
		subrole = subrole,
		subroleStrict = subroleStrict,
		coords = {
		    x = posX,
		    y = posY,
		    z = posZ,
		},
		range = range,
		showMarker = showMarker,
		showBlip = showBlip,
		blipSprite = blipSprite,
		blipColor = blipColor,
		eventName = eventName,
		eventArgs = eventArgs or {}
	}
end


function MarkerManager:GetMarkersBySource(source)
	local markers = {}
	for name, marker in pairs(self.markers) do
		if marker.role == 0 or marker.role == PlayerManager.players[source].data.role then
			if marker.subrole == 0 or (marker.subrole == PlayerManager.players[source].data.subrole and marker.subroleStrict) or (marker.subrole <= PlayerManager.players[source].data.subrole and not marker.subroleStrict) then
				markers[name] = marker
			end
		end
	end

	return markers
end