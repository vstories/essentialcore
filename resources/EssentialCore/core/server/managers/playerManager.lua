-- Constructor
PlayerManager = {}
PlayerManager.__index = PlayerManager

-- Meta table for users
setmetatable(PlayerManager, {
	__call = function(self)
		local p = {}
		
        p.table = "players"
		p.players = {}
        p.modules = {}
		p.tmp = {}
		
		return setmetatable(p, PlayerManager)
	end
})

-- Remove for the list player
function PlayerManager:RemovePlayer(source)
   self.players[source] = nil
end

-- Check if is in the list player
function PlayerManager:PlayerExist(source)
    if (self.players[source] == nil) then
        return false
    else
        return true
    end
end

-- Check if is in the list player
function PlayerManager:PlayerExistInTmp(source)
    if (self.tmp[source] == nil) then
        return false
    else
        return true
    end
end

function PlayerManager:RemovePlayerInTmp(source)
   self.tmp[source] = nil
end

-- Select player in database
function PlayerManager:SelectPlayerInDB(steamId)
    local query = MySQL:executeQuery("SELECT * FROM " .. self.table .. " WHERE steamId = '@steamId'", { ['@steamId'] = steamId } )
    local result = MySQL:getResult(query)
    
    return result
end

-- Create player in database
function PlayerManager:CreatePlayerInDB(steamId)
    local query = MySQL:executeQuery("INSERT INTO " .. self.table .. " (`steamId`) VALUES ('@steamId')", { ['@steamId'] = steamId } )
    local result = query.result

    return result
end

-- Return player data
function PlayerManager:GetPlayerData(steamId)

    local player = self:SelectPlayerInDB(steamId)
    
    if player.id == nil then
            self:CreatePlayerInDB(steamId)
            player = self:SelectPlayerInDB(steamId)
            print("[ERROR] Insertion failed for steamId : " .. steamId)
    end

    return player
end

-- Add Player in player list
function PlayerManager:AddPlayer(steamId, source)
    self.players[source] = self:LoadPlayer(source, self.tmp[steamId])
    self.players[source].source = source
    self.tmp[steamId] = nil
end

function PlayerManager:InitPlayer(steamId)

    local dataPlayer = self:GetPlayerData(steamId)
    if (not dataPlayer.ban and dataPlayer.whitelist) or (dataPlayer.permissionLevel > 0 and Config.onlyStaff) then
        self.tmp[steamId] = dataPlayer
        return true
    else
        return false
    end
end

function PlayerManager:AddModule(module)
       table.insert(self.modules, module)
end

function PlayerManager:LoadPlayer(source, dataPlayer)
    local player = Player(source, dataPlayer)
    for k, module in pairs(self.modules) do 
        player[string.lower(module)] = _G[module](player.data.id)
        player[string.lower(module)]['data'] = {}
        setmetatable(player[string.lower(module)]['data'], { __index = player.data })       
    end

    return player
end