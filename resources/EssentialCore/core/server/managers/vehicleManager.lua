-- Constructor
vehicleManager = {}
vehicleManager.__index = vehicleManager

-- Meta table for users
setmetatable(vehicleManager, {
	__call = function(self)
		local p = {}
		
		p.vehicles = {}
        p.modules = {}
		
		return setmetatable(p, vehicleManager)
	end
})

-- Remove for the list player
function vehicleManager:Addvehicle(vehicleData)
   self.vehicles[vehicleserverId] = self:Loadvehicle(dataPlayer)
end

function vehicleManager:AddModule(module, bool)
       self.modules[module] = type
end

function vehicleManager:Loadvehicle(datavehicle)
    local vehicle = vehicle(source, datavehicle) 
    for module, type in pairs(self.modules) do
        vehicle[string.lower(module)] = _G[module](vehicle.id)
        vehicle[string.lower(module)]['data'] = {}
        setmetatable(vehicle[string.lower(module)]['data'], { __index = vehicle.data })
    end
    return vehicle
end

function vehicleManagerSavevehicleById(vehicleserverId)
    if vehicles[vehicleserverId] ~= nil then
        vehicles[vehicleserverId]:save()
        return true
    end
    
    return false
end