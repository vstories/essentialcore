-- Print tables
function tprint(tbl, indent)
  if not indent then indent = 0 end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      print(formatting)
      tprint(v, indent+1)
    else
      print(formatting .. v)
    end
  end
end

-- Get SteamId
function getSteamId(source)
  local identifiers = GetPlayerIdentifiers(source)
  return identifiers[1]
end

-- Get first character
function firstString(String, Compare)
  if (string.sub(String, 1, string.len(Compare)) == Compare) then
    return true
  else
    return false
  end
end

function stringSplit(self, delimiter)
  local a = self:Split(delimiter)
  local t = {}

  for i = 0, #a - 1 do
     table.insert(t, a[i])
  end

  return t
end

function InheriteTo(_class, class)
  _class = _class or {}

  -- function for __index.
  if class then
    setmetatable(_class,{__index = class})
  end

  return _class

end