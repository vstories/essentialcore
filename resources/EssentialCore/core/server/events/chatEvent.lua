-- Recive chat message
AddEventHandler('chatMessage', function(source, n, message)
    
    -- Check if "/" is first character
    if (firstString(message, "/")) then
        
        -- Get args commandes
        local commandArgs = stringSplit(message, " ")

        -- Get name commande
        local commandName = string.gsub(commandArgs[1], "/", "")

        -- Check if command exist
        if CommandManager.commands[commandName] ~= nil and CommandManager.commands[commandName].permissionLevel ~= nil then
            local player = PlayerManager.players[source].data
            if (player.permissionLevel >= CommandManager.commands[commandName].permissionLevel) then
                CommandManager.commands[commandName].callback(source, commandArgs)
            end
        end
    
        CancelEvent()
    end
    
end)