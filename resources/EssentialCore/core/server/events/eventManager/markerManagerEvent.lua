RegisterServerEvent("ec:manager:AddMarker")
AddEventHandler("ec:manager:AddMarker", function(name, role, subrole, subroleStrict, posX, posY, posZ, range, showMarker, showBlip, blipSprite, blipColor, eventName, eventArgs)
    MarkerManager:AddMarker(name, role, subrole, subroleStrict, posX, posY, posZ, range, showMarker, showBlip, blipSprite, blipColor, eventName, eventArgs)
end)