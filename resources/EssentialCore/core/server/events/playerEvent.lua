-- Spawn player
RegisterServerEvent('playerSpawn')
AddEventHandler('playerSpawn', function()
	TriggerClientEvent('ec:player:spawnPlayer', source, PlayerManager.players[source].data.posX,  PlayerManager.players[source].data.posY,  PlayerManager.players[source].data.posZ)
end)

RegisterServerEvent('ec:player:SetPlayer')
AddEventHandler('ec:player:SetPlayer', function(playerData, SaveInDB)
    if PlayerManager.players[source] then
	    PlayerManager.players[source]:Save(playerData, SaveInDB)
        TriggerClientEvent('ec:player:SetPlayer', source, playerData)
    end
end)

RegisterServerEvent('ec:player:GetPlayer')
AddEventHandler('ec:player:GetPlayer', function()
    if PlayerManager.players[source] then      
        TriggerClientEvent('ec:player:SetPlayer', source, PlayerManager.players[source].data)
    end
end)

AddEventHandler('playerDropped', function()
    if PlayerManager:PlayerExist(source) then
        PlayerManager.players[source]:Save()
        PlayerManager:RemovePlayer(source)
    end
    
    if PlayerManager:PlayerExistInTmp(source) then
        PlayerManger.RemovePlayerInTmp(source) 
    end
end)

-- Event is call after client is 100% loaded
RegisterServerEvent('ec:player:clientJoin')
AddEventHandler('ec:player:clientJoin', function()
    local steamId = getSteamId(source)
    
    -- Add player in player table
    if not PlayerManager:PlayerExist(source) then
        PlayerManager:AddPlayer(steamId, source)
    end
    
end)

-- Event before player join
AddEventHandler("playerConnecting", function(playerName, setCallback)
    local steamId = getSteamId(source)
    -- Check player and init
    if not PlayerManager:InitPlayer(steamId) then
        setCallback("This is a private server. Ask the administrator.")
        CancelEvent()

        return
    end
end)

-- Event for kick payer
RegisterServerEvent('ec:player:kickPlayer')
AddEventHandler('ec:player:kickPlayer', function(reason)
    local player = PlayerManager.players[source]
    player:Kick(reason)
end)

-- Check ping
RegisterServerEvent("ec:player:checkPing")
AddEventHandler("ec:player:checkPing", function()
	local ping = GetPlayerPing(source)
	if ping >= Config.maxPing then
	   local player = PlayerManager.players[source]
	   player:Kick(Config.pingKickText)
	end
end)
