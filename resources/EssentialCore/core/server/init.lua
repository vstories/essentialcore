-- Init Manager --

PlayerManager = PlayerManager()
CommandManager = CommandManager()
MarkerManager = MarkerManager()

MarkerManager:AddMarker("police_station", "Police station", 0, 0, false, 436.491, -982.172, 30.699, 10, true, true, 60, 0, nil)
MarkerManager:AddMarker("hospital", "Hôpital", 0, 0, false, -449.970153808594, -340.391479492188, 34.50, 10, true, true, 61, 0, nil)
MarkerManager:AddMarker("airport", "Aéroport", 0, 0, false, -1044.64538574219, -2749.82885742188, 21.3634281158447, 10, true, true, 90, 0, nil)
MarkerManager:AddMarker("bank_1", "Banque", 0, 0, false, 436.491, -982.172, 30.699, 10, true, true, 108, 0, nil)