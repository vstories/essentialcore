-- Constructor
Player = {}
Player.__index = Player

-- Meta table for users
setmetatable(Player, {
	__call = function(self, source, data)
		local p = {}
		
		p.source = source
		p.data = data or {}
		
		return setmetatable(p, Player)
	end
})


-- Save specific data or all data
function Player:Save(data, SaveInDB)
	data = data or self.data
	SaveInDB = SaveInDB == nil and true

	local str_query = ""
	local count = 0
	
	for column, value in pairs(data) do
		if column ~= 'id' and column ~= 'steamId' then
			
			if count ~= 0 then
				str_query = str_query .. ", "
			end
			
			str_query = str_query .. tostring(column) .. " = " .. tostring(value)
			self.data[column] = value
			count = count + 1
		end
	end

	if (SaveInDB) then
		MySQL:executeQuery("UPDATE players SET " .. str_query .. " WHERE steamId = '@steamId'", { ['@steamId'] = self.data.steamId } )
	end
end


-- -- new + TODO --
-- function Player:AddHistory(idAdmin, reason, type)
-- 	--MySQL:executeQuery("INSERT INTO history (`playerId`, `idAdmin`, `type`, `reason`, `createdAt`) VALUES ('@playerId', '@idAdmin', '2', '@reason', '@createdAt')",
-- 	--	    {['@playerId'] = self.id, ['@idAdmin'] = idAdmin, ['@reason'] = reason, ['@createdAt'] = date})
-- end

-- Kicks a player with specified reason
function Player:Kick(reason)
	DropPlayer(self.source, reason)

	-- new --
	-- self.AddHistory(idAdmin, reason, 1)
end

-- -- Ban player
-- function Player:Ban(idAdmin, reason)
--     self:Kick(reason)

-- 	-- new --
--  --   self.Update("banned", 1)
-- 	-- self.AddHistory(idAdmin, reason, 2)
-- end
