-- Constructor
vehicle = {}
vehicle.__index = vehicle

-- Meta table for users
setmetatable(vehicle, {
	__call = function(self, data)
		local p = {}
		
		p.data = data or {}
		
		return setmetatable(p, vehicle)
	end
})

